﻿using System.Web;
using System.Web.Mvc;

namespace Exam_17T1021123.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
