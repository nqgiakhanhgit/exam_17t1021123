﻿using Exam_17T1021123.DataLayers;
using Exam_17T1021123.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_17T1021123.BusinessLayers
{
    public class UserAccountService
    {
        private static IUserAccountDAL UserAccountDB;
        public static void Init(DatabaseTypes dbtype, string connectingString)
        {
            switch (dbtype)
            {
                case DatabaseTypes.SQLServer:

                    UserAccountDB = new DataLayers.SQLServer.UserAccountDAL(connectingString);

                    break;
                default:
                    throw new Exception("Database Type is not supported");
            }
        }
        /// <summary>
        /// kiểm tra thông tin đăng nhập của tài khoản
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static UserAccount Authorize(string userName, string password)
        {
            return UserAccountDB.Authorize(userName, password);
        }
        public static bool ChangepassWord(string accountid, string oldpass, string newpass)
        {
            return UserAccountDB.ChangePassword(accountid, oldpass, newpass);
        }
    }
}
