﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_17T1021123.DataLayers.SQLServer
{
    public class _BaseDAL
    {
        /// </summary>
        protected string _connectionString;

        public _BaseDAL(string connectingString)
        {
            this._connectionString = connectingString;
        }


        protected SqlConnection GetConnection()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = this._connectionString;
            connection.Open();
            return connection;
        }
    }
}
