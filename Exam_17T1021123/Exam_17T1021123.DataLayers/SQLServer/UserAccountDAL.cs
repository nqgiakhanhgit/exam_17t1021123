﻿using Exam_17T1021123.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_17T1021123.DataLayers.SQLServer
{
     public class UserAccountDAL:_BaseDAL,IUserAccountDAL
    {
        public UserAccountDAL(string connectingString) : base(connectingString)
        {

        }


        public UserAccount Authorize(string userName, string password)
        {
            UserAccount data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select UserName , Passwordfrom UserAccount where UserName = @UserName AND Password = @password ";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UserName", userName);
                cmd.Parameters.AddWithValue("@password", password);


                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new UserAccount()
                        {
                            UserName = dbReader["UserName"].ToString(),

                        };
                    }
                    connection.Close();
                }
                return data;
            }
        }

        public bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            int rowsAffected = 0;
            using (SqlConnection connection = GetConnection())
            {


                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"UPDATE UserAccount SET Password=@newpass WHERE UserName = @UserName AND Password= @oldpass  ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@UserName", username);
                cmd.Parameters.AddWithValue("@newpass", newPassword);
                cmd.Parameters.AddWithValue("@oldpass", oldPassword);

                rowsAffected = Convert.ToInt32(cmd.ExecuteNonQuery());

                connection.Close();
            }

            return rowsAffected > 0;
        }
    }
}
