﻿using Exam_17T1021123.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_17T1021123.DataLayers
{
    public interface IUserAccountDAL
    {
        UserAccount Authorize(string userName, string password);

        bool ChangePassword(string accountId, string oldPassword, string newPassword);
    }
}
